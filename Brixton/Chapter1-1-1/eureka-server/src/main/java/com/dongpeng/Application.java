package com.dongpeng;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        //new SpringApplicationBuilder(Application.class).com.dongpeng.web(true).run(args);
        SpringApplication.run(Application.class, args);
    }
}
