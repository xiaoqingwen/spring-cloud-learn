package com.dongpeng.web;

import com.dongpeng.service.ComputeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConsumerController {

    @Autowired
    ComputeClient computeClient;

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public Integer add() {
        return computeClient.add(10, 20);
    }
}

//2100/26/8=10   (底薪除以26天,再除以每天8个小时,等于平常每个小时是10元)
//
//10*22*2*1.5= 660 (平常22天,每天2个小时固定加班,1.5倍)
//
//10*4*10*2=800  (四个礼拜六,40个小时乘以2倍工资)
//
//2100+660+800=3560