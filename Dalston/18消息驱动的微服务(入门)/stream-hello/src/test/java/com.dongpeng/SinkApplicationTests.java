package com.dongpeng;

import com.dongpeng.sink.SinkSender;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class SinkApplicationTests {

    @Autowired
    private SinkSender sender;

    @Test
    public void hello() throws Exception {
        System.out.println("准备发送消息。。。");
        sender.send();
    }
}
